package tp1.xaviergosselin.quiz_hockey

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.google.android.material.textfield.TextInputEditText

class MainActivity : AppCompatActivity(), View.OnClickListener {

    // variables membres
    lateinit var prefs: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    private lateinit var txt_pseudo: TextInputEditText
    private lateinit var btn_start: Button

    // Gestionnaire de la création de l'activité
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Création SharedPreferences
        prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE)
        editor = prefs.edit()
        //Ajout clé score
        editor.putInt("Score", 0)

        //Obtenir les différents composants
        txt_pseudo = findViewById(R.id.txt_changePseudo)
        btn_start = findViewById(R.id.btn_start)

        // Passage de gestion de clics au bouton démarrer
        btn_start.setOnClickListener(this)

    }

    // Gestionnaire de clics sur le bouton démarrer
    override fun onClick(v: View?) {
        val intent: Intent
        if (v != null) {
            when (v.id) {
                R.id.btn_start -> {
                    // Vérifie si le champ pseudo est vide
                    if(txt_pseudo.getText().toString() == ""){

                        // Envoie le message peuso obligatoire
                        Toast.makeText(this, R.string.pseudo_oblig, Toast.LENGTH_LONG).show()
                    }
                    else{
                        // Ajout clé pseudo
                        editor.putString("Pseudo", txt_pseudo.getText().toString())
                        editor.apply()

                        // Démarre l'activité Quiz
                        intent = Intent(this@MainActivity, QuizActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        }
    }


}
