package tp1.xaviergosselin.quiz_hockey

data class QuestionsAnswers(val question: List<String>, val reponses: List<List<String>>, val bonne_reponse: List<String>)


    val questions = QuestionsAnswers(listOf(
        "Quel Québécois a obtenu le plus grand nombre de points dans la LNH?",
        "Qui est le dernier Québécois à avoir remporté le trophée Calder remis à la recrue par excellence?",
        "Combien de Coupes Stanley les Canadiens de Montréal ont-ils remportés?",
        "Quel joueur québécois a soulevé la Coupe Stanley à 11 reprises?",
        "En quelle année les Canadiens de Montréal ont-ils atteint la finale de la Coupe Stanley pour la dernière fois?"
    ),listOf(
        listOf(
            "Guy Lafleur",
            "Marcel Dionne",
            "Mario Lemieux",
            "Luc Robitaille"
        ),
        listOf(
            "Raymond Bourque",
            "Jonathan Drouin",
            "Jonathan Huberdeau",
            "Patrice Bergeron"
        ),
        listOf(
            "24",
            "19",
            "22",
            "25"
        ),
        listOf(
            "Mario Lemieux",
            "Jean Béliveau",
            "Maurice Richard",
            "Henri Richard"
        ),
        listOf(
            "1993",
            "2014",
            "2021",
            "1998"
        )
    ),listOf(
        "Marcel Dionne",
        "Jonathan Huberdeau",
        "24",
        "Henri Richard",
        "2021"
    ))

