package tp1.xaviergosselin.quiz_hockey

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

class ConfigActivity : AppCompatActivity(), View.OnClickListener {

    // variables membres
    lateinit var prefs: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var txt_presentPseudo: TextInputLayout
    lateinit var txt_changePseudo: TextInputEditText
    lateinit var btn_changer: Button

    // Gestionnaire de la création de l'activité
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_config)

        // Définir la toolbar
        val toolbar = findViewById<Toolbar>(R.id.tbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.config)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Obtenir le SharedPreferences
        prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE)
        editor = prefs.edit()

        // Obtenir les différents composants
        txt_changePseudo = findViewById(R.id.txt_changePseudo)
        txt_presentPseudo = findViewById(R.id.txt_presentPseudo)
        btn_changer = findViewById(R.id.btn_changer)

        // Définir le composant pseudo
        var presentPseudo = prefs.getString("Pseudo","Pseudo")
        txt_presentPseudo.setHint(presentPseudo)

        // Passage de gestion de clics au bouton Changer
        btn_changer.setOnClickListener(this)
    }

    // Gestionnaire de clics sur les différents boutons
    override fun onClick(v: View?) {
        val intent: Intent
        if (v != null) {
            when (v.id) {
                R.id.btn_changer->{
                    // Vérifie si le champ pseudo est vide
                    if(txt_changePseudo.getText().toString() == ""){

                        // Envoie le message peuso obligatoire
                        Toast.makeText(this, R.string.pseudo_oblig, Toast.LENGTH_LONG).show()
                    }
                    else{
                        // Modifie la valeur de la clé pseudo du SharePreferences
                        editor.putString("Pseudo", txt_changePseudo.getText().toString())
                        editor.apply()

                        // Envoie le message peuso changé
                        Toast.makeText(this, R.string.pseudo_changer, Toast.LENGTH_LONG).show()

                        // Démarre l'activité Quiz
                        intent = Intent(this@ConfigActivity, QuizActivity::class.java)
                        startActivity(intent)
                    }
                }
            }
        }
    }


}