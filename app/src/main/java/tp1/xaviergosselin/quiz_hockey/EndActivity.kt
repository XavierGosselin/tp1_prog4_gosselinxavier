package tp1.xaviergosselin.quiz_hockey

import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class EndActivity : AppCompatActivity(), View.OnClickListener {

    // variables membres
    private lateinit var txt_pseudo: TextView
    private lateinit var txt_score: TextView
    private lateinit var btn_rejouer: Button
    private lateinit var btn_partager: Button
    lateinit var prefs: SharedPreferences

    // Gestionnaire de la création de l'activité
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_end)

        // Obtenir le SharedPreferences
        prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE)

        // Obtenir et définir le composant pseudo
        txt_pseudo = findViewById(R.id.pseudo_joueur)
        var pseudo = prefs.getString("Pseudo","Pseudo")
        txt_pseudo.setText(pseudo)

        // Obtenir et définir le composant score
        txt_score = findViewById(R.id.score)
        var score = prefs.getInt("Score", 0)
        var message_score = getString(R.string.score) + " $score/5"
        txt_score.setText(message_score)

        // Obtenir les composants boutons
        btn_rejouer = findViewById(R.id.rejouer)
        btn_partager = findViewById(R.id.partager)

        // Passage de gestion de clics aus boutons
        btn_rejouer.setOnClickListener(this)
        btn_partager.setOnClickListener(this)
    }

    // Gestionnaire de clics sur les différents boutons
    override fun onClick(v: View?) {
        val intent: Intent
        if (v != null) {
            when (v.id) {
                R.id.rejouer -> {

                    // Démarre l'activité Main
                    intent = Intent(this@EndActivity, MainActivity::class.java)
                    startActivity(intent)
                }
                R.id.partager -> {

                    // Partage le score
                    partagerScore()
                }
            }
        }
    }

    // Gestionnaire du partage de score
    private fun partagerScore() {

        // Obtenir la valeur de la clé score du SharedPreferences
        var score = prefs.getInt("Score", 0)

        //Démarre l'application choisi pour partager son score
        intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        intent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.texto))
        intent.putExtra(Intent.EXTRA_TEXT, getString(R.string.texto) + ": $score")
        startActivity(intent)
    }
}

