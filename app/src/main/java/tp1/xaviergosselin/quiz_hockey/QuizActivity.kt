package tp1.xaviergosselin.quiz_hockey

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.widget.Toolbar


class QuizActivity : AppCompatActivity(), View.OnClickListener {

    // variables membres
    lateinit var prefs: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    private lateinit var ivImage: ImageView
    private lateinit var tvQuestion: TextView
    private lateinit var btn_r1 :Button
    private lateinit var btn_r2 :Button
    private lateinit var btn_r3 :Button
    private lateinit var btn_r4 :Button
    private lateinit var btn_valider :Button
    private lateinit var progressBar: ProgressBar
    private lateinit var currentProgress: TextView
    private var currentPosition = 0
    private lateinit var clickedButton: Button
    private val imagesList =
        listOf(R.drawable.question1, R.drawable.question2, R.drawable.question3, R.drawable.question4, R.drawable.question5)

    // Gestionnaire de la création de l'activité
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_quiz)

        // Définir toolbar
        val toolbar = findViewById<Toolbar>(R.id.tbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.app_name)

        // Obtenir le SharedPreferences
        prefs = getSharedPreferences("MonFichierDeSauvegarde", MODE_PRIVATE)
        editor = prefs.edit()

        // Obtenir les différents composants
        ivImage = findViewById(R.id.iv_image)
        tvQuestion = findViewById(R.id.question)
        btn_r1 = findViewById(R.id.reponse1)
        btn_r2 = findViewById(R.id.reponse2)
        btn_r3 = findViewById(R.id.reponse3)
        btn_r4 = findViewById(R.id.reponse4)
        btn_valider = findViewById(R.id.valider)
        progressBar = findViewById(R.id.pg_image)
        currentProgress = findViewById(R.id.tv_currentProgress)

        // Si saveInsantanceState n'est pas nul, va chercher la posisition courante
        if (savedInstanceState != null) {
            currentPosition = savedInstanceState.getInt("currentPosition")
        }

        // Définir les différents composants selon la position courante
        ivImage.setImageResource(imagesList[currentPosition % imagesList.size])
        tvQuestion.setText(questions.question[currentPosition % questions.question.size])
        progressBar.max = imagesList.size - 1
        currentProgress.setText((currentPosition +1).toString() +"/5")
        btn_r1.setText(questions.reponses[currentPosition % questions.question.size][0])
        btn_r2.setText(questions.reponses[currentPosition % questions.question.size][1])
        btn_r3.setText(questions.reponses[currentPosition % questions.question.size][2])
        btn_r4.setText(questions.reponses[currentPosition % questions.question.size][3])
        btn_valider.setText(getString(R.string.valider))

        // Passage de gestion de clics aux différents boutons
        btn_r1.setOnClickListener(this)
        btn_r2.setOnClickListener(this)
        btn_r3.setOnClickListener(this)
        btn_r4.setOnClickListener(this)
        btn_valider.setOnClickListener(this)

    }

    // Gestionnaire de clics sur les différents boutons
    override fun onClick(v: View?) {
        val intent: Intent

        // Remet le bouton des réponses à leur état d'origine
        resetReponse()
        if (v != null) {
            when (v.id) {
                // Pour chaque clique, change le background pour indiquer que le bouton est cliqué
                // et définie le la variable clickedButton
                R.id.reponse1 -> {
                    btn_r1.setBackgroundColor(Color.parseColor("#1B5E20"))
                    btn_r1.setTextColor(Color.WHITE)
                    clickedButton = btn_r1
                }
                R.id.reponse2 -> {
                    btn_r2.setBackgroundColor(Color.parseColor("#1B5E20"))
                    btn_r2.setTextColor(Color.WHITE)
                    clickedButton = btn_r2
                }
                R.id.reponse3 -> {
                    btn_r3.setBackgroundColor(Color.parseColor("#1B5E20"))
                    btn_r3.setTextColor(Color.WHITE)
                    clickedButton = btn_r3
                }
                R.id.reponse4 -> {
                    btn_r4.setBackgroundColor(Color.parseColor("#1B5E20"))
                    btn_r4.setTextColor(Color.WHITE)
                    clickedButton = btn_r4
                }
                R.id.valider -> {
                    // Vérifie si le bouton est "Valider"
                    if(btn_valider.getText().toString() == getString(R.string.valider)){

                        // Valide la réponse
                        validerQuestions()

                        // Vérifie s'il reste une ou des questions
                        if(currentPosition <= questions.question.size - 2 ){

                            // Modifie le texte du bouton
                            btn_valider.setText(getString(R.string.next))
                        }
                        // Modifie le texte du bouton
                        else{btn_valider.setText(getString(R.string.terminer))}
                    }
                    else{
                        // Vérifie s'il reste une ou des questions
                        if(currentPosition <= questions.question.size - 2 ){

                            // charge la nouvelle question
                            loadNewQuestion()
                        }
                        else{
                            // Démarre l'activité finale
                            intent = Intent(this@QuizActivity, EndActivity::class.java)
                            startActivity(intent)
                        }
                    }
                }
            }
        }
    }

    // Gestionnaire de la remise en état d'origine du bouton des réponses
    private fun resetReponse(){
        btn_r1.setBackgroundColor(Color.WHITE)
        btn_r1.setTextColor(Color.parseColor("#1B5E20"))
        btn_r2.setBackgroundColor(Color.WHITE)
        btn_r2.setTextColor(Color.parseColor("#1B5E20"))
        btn_r3.setBackgroundColor(Color.WHITE)
        btn_r3.setTextColor(Color.parseColor("#1B5E20"))
        btn_r4.setBackgroundColor(Color.WHITE)
        btn_r4.setTextColor(Color.parseColor("#1B5E20"))
    }

    // Gestionnaire du chargement de la nouvelle question
    private fun loadNewQuestion(){
        // Augmente la position courante
        currentPosition++

        // Définir les différents composants selon la position courante
        ivImage.setImageResource(imagesList[currentPosition % imagesList.size])
        tvQuestion.setText(questions.question[currentPosition % questions.question.size])
        progressBar.progress = (currentPosition % imagesList.size)
        currentProgress.setText((currentPosition +1).toString() +"/5")
        btn_r1.setText(questions.reponses[currentPosition % questions.question.size][0])
        btn_r2.setText(questions.reponses[currentPosition % questions.question.size][1])
        btn_r3.setText(questions.reponses[currentPosition % questions.question.size][2])
        btn_r4.setText(questions.reponses[currentPosition % questions.question.size][3])
        btn_valider.setText(getString(R.string.valider))
    }

    // Gestionnaire de la validation de la réponse
    private fun validerQuestions() {
        when(currentPosition){
            0->{
                // Vérifie si le bouton cliqué est la bonne réponse
                if(clickedButton.getText().toString() == questions.bonne_reponse[0]){

                    // Change le background en vert pour indiquer que la réponse est bonne
                    clickedButton.setBackgroundColor(Color.parseColor("#FFB2FF59"))

                    // Modifie la valeur de la clé score du SharePreferences
                    var score = prefs.getInt("Score", 0)
                    editor.putInt("Score", score + 1)
                    editor.apply()
                }
                else{
                    // Change le background en rouge pour indiquer que la réponse est mauvaise
                    clickedButton.setBackgroundColor(Color.parseColor("#FFFF5252"))
                    clickedButton.setTextColor(Color.WHITE)

                    // Change le background en vert pour indiquer la bonne réponse
                    btn_r2.setBackgroundColor(Color.parseColor("#FFB2FF59"))
                }
            }
            1->{
                // Vérifie si le bouton cliqué est la bonne réponse
                if(clickedButton.getText().toString() == questions.bonne_reponse[1]){

                    // Change le background en vert pour indiquer que la réponse est bonne
                    clickedButton.setBackgroundColor(Color.parseColor("#FFB2FF59"))

                    // Modifie la valeur de la clé score du SharePreferences
                    var score = prefs.getInt("Score", 0)
                    editor.putInt("Score", score + 1)
                    editor.apply()
                }
                else{
                    // Change le background en rouge pour indiquer que la réponse est mauvaise
                    clickedButton.setBackgroundColor(Color.parseColor("#FFFF5252"))
                    clickedButton.setTextColor(Color.WHITE)

                    // Change le background en vert pour indiquer la bonne réponse
                    btn_r3.setBackgroundColor(Color.parseColor("#FFB2FF59"))
                }
            }
            2->{
                // Vérifie si le bouton cliqué est la bonne réponse
                if(clickedButton.getText().toString() == questions.bonne_reponse[2]){

                    // Change le background en vert pour indiquer que la réponse est bonne
                    clickedButton.setBackgroundColor(Color.parseColor("#FFB2FF59"))

                    // Modifie la valeur de la clé score du SharePreferences
                    var score = prefs.getInt("Score", 0)
                    editor.putInt("Score", score + 1)
                    editor.apply()
                }
                else{
                    // Change le background en rouge pour indiquer que la réponse est mauvaise
                    clickedButton.setBackgroundColor(Color.parseColor("#FFFF5252"))
                    clickedButton.setTextColor(Color.WHITE)

                    // Change le background en vert pour indiquer la bonne réponse
                    btn_r1.setBackgroundColor(Color.parseColor("#FFB2FF59"))
                }
            }
            3->{
                // Vérifie si le bouton cliqué est la bonne réponse
                if(clickedButton.getText().toString() == questions.bonne_reponse[3]){

                    // Change le background en vert pour indiquer que la réponse est bonne
                    clickedButton.setBackgroundColor(Color.parseColor("#FFB2FF59"))

                    // Modifie la valeur de la clé score du SharePreferences
                    var score = prefs.getInt("Score", 0)
                    editor.putInt("Score", score + 1)
                    editor.apply()
                }
                else{
                    // Change le background en rouge pour indiquer que la réponse est mauvaise
                    clickedButton.setBackgroundColor(Color.parseColor("#FFFF5252"))
                    clickedButton.setTextColor(Color.WHITE)

                    // Change le background en vert pour indiquer la bonne réponse
                    btn_r4.setBackgroundColor(Color.parseColor("#FFB2FF59"))
                }
            }
            4->{
                // Vérifie si le bouton cliqué est la bonne réponse
                if(clickedButton.getText().toString() == questions.bonne_reponse[4]){

                    // Change le background en vert pour indiquer que la réponse est bonne
                    clickedButton.setBackgroundColor(Color.parseColor("#FFB2FF59"))

                    // Modifie la valeur de la clé score du SharePreferences
                    var score = prefs.getInt("Score", 0)
                    editor.putInt("Score", score + 1)
                    editor.apply()
                }
                else{
                    // Change le background en rouge pour indiquer que la réponse est mauvaise
                    clickedButton.setBackgroundColor(Color.parseColor("#FFFF5252"))
                    clickedButton.setTextColor(Color.WHITE)

                    // Change le background en vert pour indiquer la bonne réponse
                    btn_r3.setBackgroundColor(Color.parseColor("#FFB2FF59"))
                }
            }
        }
    }

    // Initialisation du menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar_menu, menu)
        return true
    }

    // Gestion des clics sur les items du menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.mn_config -> {
                Toast.makeText(this, "Configuration", Toast.LENGTH_SHORT).show()
                intent = Intent(this, ConfigActivity::class.java)
                startActivity(intent)
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    // Gestionnaire de l'affectation de la position courante
    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        currentPosition = savedInstanceState.getInt("currentPosition") ?: 0
    }

    // Gestionnaire de la sauvegarde de la position courante
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("currentPosition", currentPosition)
    }
}
